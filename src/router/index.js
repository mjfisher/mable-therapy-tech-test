import Vue from "vue";
import VueRouter from "vue-router";
import SearchContainer from "../components/search/SearchContainer.vue";
import UserContainer from "../components/users/UserContainer.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/:query?",
    name: "search",
    component: SearchContainer,
    props: true
  },
  {
    path: "/users/:login",
    name: "user",
    component: UserContainer,
    props: true
  }
];

const router = new VueRouter({
  routes
});

export default router;
