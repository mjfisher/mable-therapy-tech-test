import axios from "axios";

async function search(query) {
  const result = await axios.get(
    `https://api.github.com/search/users?q=${query}`
  );
  return result.data.items;
}

async function user(login) {
  const result = await axios.get(`https://api.github.com/users/${login}`);
  return result.data;
}

async function repos(login) {
  const result = await axios.get(`https://api.github.com/users/${login}/repos`);
  return result.data;
}

async function events(login) {
  const result = await axios.get(
    `https://api.github.com/users/${login}/events`
  );
  return result.data;
}

export default {
  events,
  search,
  repos,
  user
};
