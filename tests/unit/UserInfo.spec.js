import Vue from "vue";
import Buefy from "buefy";
import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";

import UserInfo from "@/components/users/UserInfo.vue";

Vue.config.productionTip = false;
Vue.use(Buefy);

describe("UserInfo.vue", () => {
  let wrapper;
  const userData = {
    name: "Mark",
    location: "Illinois",
    company: "Software Inc",
    followers: 123,
    following: 321
  };

  before(() => {
    wrapper = shallowMount(UserInfo, {
      propsData: {
        user: userData
      }
    });
  });

  it("displays the user's name", () => {
    expect(wrapper.text()).to.include(userData.name);
  });

  it("displays the user's location", () => {
    expect(wrapper.text()).to.include(userData.location);
  });

  it("displays the user's company", () => {
    expect(wrapper.text()).to.include(userData.company);
  });

  it("displays the user's follower count", () => {
    expect(wrapper.text()).to.include(userData.followers);
  });

  it("displays the user's following count", () => {
    expect(wrapper.text()).to.include(userData.following);
  });
});
