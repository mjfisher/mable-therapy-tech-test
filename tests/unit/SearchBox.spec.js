import Vue from "vue";
import Buefy from "buefy";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import SearchBox from "@/components/search/SearchBox.vue";

Vue.config.productionTip = false;
Vue.use(Buefy);

describe("SearchContainer.vue", () => {
  it("sets the search input to query prop", () => {
    const query = "hello";
    const wrapper = mount(SearchBox, {
      propsData: { query: query }
    });
    const input = wrapper.find("input");
    expect(input.element.value).to.eq(query);
  });
});
