# Mable Therapy Tech Test

As per the spec, more or less. There are a number of areas where I'd want to see improvement if this was production code; I ended up time-limited on this, so some nice-to-haves didn't make it in.

Some thoughts, in no particular order:

* Vue is still relatively new to me; I'm finding the experience more enjoyable than React, but there may be some rough edges.
* I spent a little more time than I should going backwards and forwards on the structure of the routing. I didn't know if you'd consider it an important feature of this test, but the problem lends itself quite nicely to make decent use of good routing. There's always a good discussion to be had at what level you couple components to a router (or not). Happy to have that in person if we get that far!
* I've made the usual separation between presentational and container components; container components have the `Container` suffix. I've seen a quite a few different conventions for how that distinction is made in various codebases. I don't have strong opinions about what that is, as long as it is clear.
* The tests included are a token effort to show that I'm capable of writing them, more than anything. In production code I like to see quite healthy coverage.
* The UI is functional, but missing a lot of feedback that I'd consider necessary to make it 'good' - especially communicating loading states to the user, and making the distinction between 'not yet loaded' and 'zero results returned' clear.
* Error handling in this project is non-existent; it's fiddly to implement well, and I didn't think you'd get much extra benefit from seeing the extra `try/catch/catch()` and control flow everywhere, so I left it out.
* I would normally rebase a lot of the smaller commits in before merging feature branches; I've avoiding doing that in this instance to provide a bit more transparency about how I'm working.
* Hosting is done at [https://mable-therapy-tech-test.highcairn.com](https://mable-therapy-tech-test.highcairn.com) - I've already got a small kubernetes cluster running there, so it seemed a little simpler than Github pages

I hope I get to speak to you in person at some point soon. Both the company and the scope of the contract look quite exciting - if I can offer some useful skills to the team I'd be very glad to help you out.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Builds Docker image
```
npm run build:docker
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Check prettier compliance

Primarily for CI:
```
npm run prettier-check
```
